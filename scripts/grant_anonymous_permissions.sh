#!/bin/bash

# This script will grant the anonymous user permissions to execute the
# rule filtering server-sid script (and nothing else). For it to work,
# you have to create a symlink to caosdb/utils/caosdb_admin.py in this
# directory.

python3 caosdb_admin.py grant_role_permissions anonymous "SCRIPTING:EXECUTE:filter_rules.py"
