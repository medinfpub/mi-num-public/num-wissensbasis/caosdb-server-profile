# -*- coding: utf-8 -*-
#
# ** header v3.0
#
# Copyright (C) 2021 University Medical Center Göttingen, Department
# of Medical Informatics
# Copyright (C) 2021 Florian Spreckelsen
# <florian.spreckelsen@med.uni-goettingen.de>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with this program. If not, see
# <https://www.gnu.org/licenses/>.
#
# ** end header
#
"""This script will clean all rules of a given category, and remove
the references pointing to them from all other entities. **Use with
extreme caution.**.

"""

import argparse

import caosdb as db


def _delete_entity_and_references(ent, update=True):
    """Delete the given entity and all references pointing to it from
    other entities. If the deleted references were the only values of
    the corresponding reference properties, these properties are
    removed to. If update is set to False, the updated referencing
    entities are returned instead of being updated directly. The
    entity is not deleted in this case.

    Parameters:
    -----------
    ent : caosdb.Entity
        entity to be deleted
    update : bool, optional
        If True, the referencing entities are updated directly and the
        given entity is deleted. If set to False, a dry-run is
        executed instead: the referencing entity objects are modified
        and returned, instead of being updated. The given entity is
        not deleted. Default is True.

    Returns:
    --------
    referencing_entities or None
        If update is True, nothing is returned. If it is False, the
        list of the modified entities, in which all references to the
        given entities have been removed, is returned.

    """
    referencing_entities = db.execute_query(
        "FIND ENTITY WHICH REFERENCES {}".format(ent.id))

    for ref in referencing_entities:
        # TODO: Find reference properties and delete them
        for prop in ref.properties:
            if prop.datatype.startswith("LIST") and ent.id in prop.value:
                prop.value.remove(ent.id)
                if len(prop.value) == 0:
                    # delete if list is empty
                    ref.properties.remove(prop)
            else:
                if prop.value == ent.id:
                    ref.properties.remove(prop)

    if update:

        try:
            referencing_entities.update()
            ent.delete()
        except db.TransactionError as e:
            print(referencing_entities)

    else:
        return referencing_entities


def _delete_all_rules(category_name):
    """Collect all rules from one category, and delete them together with
    all references pointing to them. If the corresponding
    interpretation record references no rules anymore, delete it
    too.

    Parameters:
    -----------
    category_name : str
       name of rule category to be deleted. Rule records are found by
       executing the query `FIND RECORD <categoty_name>`.
    """

    rules = db.execute_query("FIND RECORD {}".format(category_name))

    for rule in rules:

        referencing_entities = _delete_entity_and_references(
            rule, update=False)

        for ref in referencing_entities:

            if not "Rule" in [prop.name for prop in ref.properties]:

                ref.retrieve().delete()

                referencing_entities.remove(ref)

        if len(referencing_entities) > 0:
            referencing_entities.update()
        rule.delete()


def main():
    """Take the command line input as the categories to be deleted,
    confirm the deletion, and delete all rules of the given categories.

    """
    parser = argparse.ArgumentParser()
    parser.add_argument("categories", type=str, nargs='+',
                        help="List of categories to be deleted.")
    parser.add_argument(
        "-q", "--quiet", help="Delete without confirmation.", action="store_true")
    args = parser.parse_args()

    for cat in args.categories:

        if not args.quiet:

            answ = input(
                """
This will remove all rules of category {} from the database. 
Do you really want to continue? [y/N]""".format(cat)).lower()

            if not answ == 'y':

                continue

        _delete_all_rules(cat)


if __name__ == "__main__":

    main()
