# -*- coding: utf-8 -*-
#
# ** header v3.0
#
# Copyright (C) 2021 University Medical Center Göttingen, Department
# of Medical Informatics
# Copyright (C) 2021 Florian Spreckelsen
# <florian.spreckelsen@med.uni-goettingen.de>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with this program. If not, see
# <https://www.gnu.org/licenses/>.
#
# ** end header
#
"""This script reads in the templates for our knowledge base, creates
rule records, and the corresponding publication records. It can also
be used for Stefan's tables using a `--stefan` argument.
"""

import argparse
import openpyxl
from datetime import date

import caosdb as db
from caosadvancedtools.cfood import (assure_has_parent,
                                     assure_object_is_in_list,
                                     assure_property_is,
                                     get_ids_for_entities_with_names)
from caosadvancedtools.guard import global_guard, UPDATE

# Sheet names of rule table and publication list in templates:
RULE_SHEET_NAME = "Empfehlungen"
PUBLICATION_SHEET_NAME = "Quellen"


# Allow all insertions and updates
global_guard.level = UPDATE

# Gather Record Types and properties. This also is an implicit sanity
# check for the data model.

# Publications
PUB_RT = db.RecordType(name="Publication").retrieve()
ID_PROP = db.Property(name="OriginalID").retrieve()
TITLE_PROP = db.Property(name="Title").retrieve()
DOI_PROP = db.Property(name="DOI").retrieve()
URL_PROP = db.Property(name="URL").retrieve()

# Rule properties
RULE_TEXT_PROP = db.Property(name="RuleText").retrieve()
RULE_FROM_PUB_RT = db.RecordType(name="RulesFromPublication").retrieve()
CREATED_PROP = db.Property(name="CreatedOn").retrieve()
EDITED_PROP = db.Property(name="EditedOn").retrieve()
ACTIVE_PROP = db.Property(name="Active").retrieve()


def _get_column_indices(row, column_names):
    """Get the indices of the columns with the given names within the
    given row. Return a dictionary with the columnnames as keys and
    indices as values.

    Parameters:
    -----------
    row : openpyxl row object
        row in which the indices of the columns are to be determined.
    column_names : str or list<str>
        column name or list of column names the indices of which will
        be determined.

    Returns:
    --------
    index_dict : dict
       dictionary of the form `column_name`: `index`

    """
    if isinstance(column_names, str):
        # cast to list if only one name is given.
        column_names = [column_names]

    index_dict = {}
    for ii, cell in enumerate(row):
        if cell.value in column_names:
            index_dict[cell.value] = ii

    return index_dict


def _retrieve_or_insert_publications(publication_table, mode="template"):
    """Collect all publications from a given table and return a dictionary
    with the corresponding publication records. If they don't exist
    already in the data base, the publication records are inserted.

    Parameters:
    -----------
    publication_table : openpyxl.workbook.sheet
        Table with the publication entries as rows. The first row is
        considered the header. Each publication must have a title and
        may have additional information like DOI, URL, or the original
        index if mode is `stefan`.
    mode : str, optional
       Either 'template' or 'stefan', depending on the original
       tables. This has influence on the column names that are used in
       `publication_table`, and whether an original id is
       used. Default is 'template'.

    Returns:
    --------
    publication_dict : dict
       Dictionary with the names of the publication records as keys
       and the actual records as values.
    """
    if mode.lower() not in ["template", "stefan"]:

        raise ValueError(
            "`mode` must be either `template` or `stefan`, not {}.".format(mode))

    if mode == "template":
        # first column is DOI, second is title, third is URL:
        indices_in_pub_table = {
            "DOI": 0,
            "Publikation": 1,
            "URL": 2
        }
    elif mode == "stefan":
        # determine indices from column names
        indices_in_pub_table = _get_column_indices(
            list(publication_table.rows)[0],
            ["DOI", "Index", "Publikation"]
        )

    publication_dict = {}
    for row in list(publication_table.rows)[1:]:

        if row[indices_in_pub_table["Publikation"]].value is None:
            # skip empty rows (or incomplete publications that don't
            # have a title -- should be ignored anyway)
            continue

        title = row[indices_in_pub_table["Publikation"]].value
        doi = row[indices_in_pub_table["DOI"]].value
        if "Index" in indices_in_pub_table:
            idx = int(row[indices_in_pub_table["Index"]].value)
        else:
            idx = None
        if "URL" in indices_in_pub_table:
            url = row[indices_in_pub_table["URL"]].value
        else:
            url = None

        # First try to find existing publications by DOI or title
        query = "FIND Publication WITH {}=\"{}\""
        if doi:
            query = query.format(DOI_PROP.name, doi)
        else:
            query = query.format(TITLE_PROP.name, title)

        print(query)
        try:
            pub = db.execute_query(query, unique=True)
        except db.EmptyUniqueQueryError:
            # create new publication entry
            # construct name string from possible index and first words of title
            if idx is not None:
                name = "{}_".format(idx)
            else:
                name = ''
            name += '_'.join(title.split(' ')[:6])
            print("Creating publication {}".format(name))
            pub = db.Record(name=name).add_parent(PUB_RT)
            pub.add_property(name=TITLE_PROP.name, value=title)
            if doi:
                pub.add_property(name=DOI_PROP.name, value=doi)
            if url:
                pub.add_property(name=URL_PROP.name, value=url)
            if idx is not None:
                pub.add_property(name=ID_PROP.name, value=idx)
            try:
                # Final try if the publication already exists (but
                # maybe the DOI was changed in the webinterface)
                pub.retrieve()
            except:
                # if not, insert
                pub.insert()
        publication_dict[pub.name] = pub

    return publication_dict


def _insert_rules(rule_table, publication_dict, mode="template",
                  project=None, external_tags=[], force_new_categories=False):
    """Insert rules from `rule_table` with the corresponding categories, and
    link them to the publications in `publication_dict`.

    Parameters:
    -----------
    rule_table : openpyxl.workbook.sheet
        Either the sheet containing the rules from the Compass
        template or Stefan's rule table if `mode` is "stefan".
    publication_dict : dict
        Dictionary with publication names as keys and publication
        records as values as returned by
        `_retrieve_or_insert_publications`.
    mode : str, optional
        Either "template" or "stefan". Decides which value is used to
        connect rules and publication. For "template", the title is
        used, for "stefan", the original index. Default is "template".
    project : db.Record or None
        If provided, all inserted rules will have a reference to this
        project record. Default is None.
    external_tags : list<str>, optional
        List of names of additional categories that will be appended
        to all inserted rules. Default is [].
    force_new_categories : bool, optional
        Whether the script should insert missing categories without
        asking. Default is False.

    """
    if mode.lower() not in ["template", "stefan"]:
        raise ValueError(
            "`mode` must be either `template` or `stefan`, not {}.".format(mode))
    elif mode.lower() == "template":
        identifier = TITLE_PROP.name
        indices_in_rule_table = {
            "source": 0,
            "text": 1,
            "tags": 2
        }
        interpreter = None
    elif mode.lower() == "stefan":
        identifier = ID_PROP.name
        indices_in_rule_table = {
            "index": 0,
            "text": 2,
            "tags": 3
        }
        # Get Stefan
        interpreter = db.execute_query(
            "FIND person WITH name=\"Vogel, Stefan\"", unique=True)
    today = date.today()

    # collect all possible rule types here for performance reasons
    rule_types = {rule_type.name.lower(): rule_type for rule_type in db.execute_query(
        "FIND RECORDTYPE Rule")}

    for row in list(rule_table.rows)[1:]:

        if not row[indices_in_rule_table["text"]].value:
            # Skip empty rows
            continue
        if mode == "template":
            pub_identifier = "source"
        else:
            pub_identifier = "index"
        pubs = []
        for pub in publication_dict.values():
            # possibly more than one source, separated by commas
            if isinstance(row[indices_in_rule_table[pub_identifier]].value, int):
                control = [row[indices_in_rule_table[pub_identifier]].value]
            else:
                control = row[indices_in_rule_table[pub_identifier]
                              ].value.split(',')
            if pub.get_property(identifier).value in control:
                pubs.append(pub)
        rfps = {}
        for pub in pubs:
            try:
                rfp = db.execute_query(
                    "FIND {} WHICH REFERENCES {}".format(
                        RULE_FROM_PUB_RT.name, pub.id),
                    unique=True
                )
            except db.EmptyUniqueQueryError:
                rfp = db.Record().add_parent(RULE_FROM_PUB_RT)
                rfp.add_property(name=PUB_RT.name, value=pub.id)
                rfp.insert()
            rfps[pub.name] = rfp

        tagnames = row[indices_in_rule_table["tags"]
                       ].value.split(',') + external_tags
        parents = []
        for tag in tagnames:
            if tag.strip().lower() in rule_types:
                parents.append(rule_types[tag.strip().lower()])
            else:
                answ = input("There is no rule category '{}'.  "
                             "Shall a generic category of this name "
                             "be inserted into the database? [y/N]".format(tag.strip()))
                if answ.lower() == 'y':
                    new_type = db.RecordType(name=tag.strip())
                    new_type.add_parent(rule_types["rule"])
                    new_type.insert()
                    parents.append(new_type)
                    rule_types[new_type.name.lower()] = new_type

        if not parents:
            # no tags are specified, so this is a generic rule
            parents = [rule_types["rule"]]
        text = row[indices_in_rule_table["text"]].value
        print(text)
        # either find existing rule and make sure it has all given
        # categories, or create a new one.
        to_be_updated = db.Container()
        try:
            rule = db.execute_query("FIND RECORD Rule WITH {}=\"{}\"".format(
                RULE_TEXT_PROP.name, text), unique=True)
            for parent in parents:
                assure_has_parent(rule, parent.name,
                                  to_be_updated=to_be_updated)
        # Either there is no rule for this case or there are certain
        # evil combinations of very long texts ending on special
        # characters that may lead to a failing query so just check
        # for a generic CaosDBException. See
        # https://gitlab.com/caosdb/caosdb-server/-/issues/101
        except db.CaosDBException:
            rule = db.Record().add_property(name=RULE_TEXT_PROP.name, value=text)
            for parent in parents:
                rule.add_parent(parent)
            # new rules are both edited and created today
            rule.add_property(name=CREATED_PROP.name, value=today)
            rule.add_property(name=EDITED_PROP.name, value=today)
            rule.insert()
        # all rules that are imported should be active
        assure_property_is(rule, ACTIVE_PROP.name, True,
                           to_be_updated=to_be_updated)
        if project:
            assure_object_is_in_list(project, rule, "Project",
                                     to_be_updated=to_be_updated)
        if to_be_updated:
            assure_property_is(rule, EDITED_PROP.name, today,
                               to_be_updated=to_be_updated)
        for rfp in rfps.values():
            assure_object_is_in_list(
                rule, rfp, "Rule", to_be_updated=to_be_updated)
            if mode == "stefan":
                assure_object_is_in_list(interpreter, rfp, "Interpreter",
                                         to_be_updated=to_be_updated)
        get_ids_for_entities_with_names(to_be_updated)
        if to_be_updated:
            to_be_updated.update(unique=False)


def main():
    """Read in the command line arguments and trigger the corresponding
    import functions.

    """
    parser = argparse.ArgumentParser()
    parser.add_argument("-t", "--template", type=str, nargs=1,
                        help="Filename of template to be read in.")
    parser.add_argument("--stefan", type=str, nargs=2,
                        help="Special treatment for Stefan's tables. "
                        "Enter two filenames, the first of which contains "
                        "the rules, the second, the publications.")
    parser.add_argument("--tags", type=str, nargs='+',
                        help="Additional tags that will be given to all imported rules")
    parser.add_argument("--project", type=str, nargs=1,
                        help="Optional name of the project to which the imported rules belong.")
    parser.add_argument("-f", "--force", action="store_true",
                        help="Whether missing categories shall be inserted without asking.")
    args = parser.parse_args()

    if args.project:
        # Look for the given project. `unique=True` ensures that
        # errors are raised if it is ambiguous or if there is none.
        project = db.execute_query(
            "FIND Project WITH name={}".format(args.project[0]), unique=True)
    else:
        project = None

    external_tags = []
    if args.tags:
        # Retrieve tags and make sure they actually are RecordTypes
        # that are children of `Rule`. An error is raised by the
        # retreival if a tag doesn't exist or if it is ambiguous.
        for tag in args.tags:
            external_tags.append(
                db.execute_query(
                    "FIND RecordType Rule WITH name=\"{}\"".format(tag),
                    unique=True
                ).name
            )

    if not args.template and not args.stefan:
        # no input file whatsoever
        raise IOError("You must specify at least one input file using `--template` or `--stefan`."
                      "  Execute with `--help` for more information.")

    elif args.template:

        template_file = openpyxl.load_workbook(args.template[0])
        rule_table = template_file[RULE_SHEET_NAME]
        publication_table = template_file[PUBLICATION_SHEET_NAME]
        publication_dict = _retrieve_or_insert_publications(publication_table)
        _insert_rules(rule_table, publication_dict, project=project,
                      external_tags=external_tags,
                      force_new_categories=args.force)

    elif args.stefan:

        rule_table = openpyxl.load_workbook(args.stefan[0])
        rule_table = rule_table["Harmonisierung"]
        publication_table = openpyxl.load_workbook(args.stefan[1])
        publication_table = publication_table[publication_table.sheetnames[0]]
        publication_dict = _retrieve_or_insert_publications(
            publication_table, mode="stefan")
        _insert_rules(rule_table, publication_dict, mode="stefan",
                      project=project, external_tags=external_tags,
                      force_new_categories=args.force)


if __name__ == "__main__":

    main()
