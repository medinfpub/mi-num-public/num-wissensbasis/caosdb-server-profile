# -*- coding: utf-8 -*-
#
# ** header v3.0
#
# Copyright (C) 2020 University Medical Center Göttingen, Department
# of Medical Informatics
# Copyright (C) 2020 Florian Spreckelsen
# <florian.spreckelsen@med.uni-goettingen.de>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with this program. If not, see
# <https://www.gnu.org/licenses/>.
#
# ** end header
#
from caosmodels.parser import parse_model_from_yaml


def main():
    model = parse_model_from_yaml("datamodel.yml")
    model.sync_data_model()


if __name__ == "__main__":

    main()
