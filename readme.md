# CaosDB server profile #

This is the server profile for the NUM projects Compass, egePan
Unimed, and B-FAST.

## Setup ## 

### Setup using the sources ###

Get the CaosDB sources from https://gitlab.com/caosdb and copy the
contents of `profile/custom/caosdb-server` into the corresponding
directories of the sources (see
[here](https://docs.indiscale.com/caosdb-webui/extension.html) for
more information on custom extensions. Follow the installation and
configuration instructions for the [CaosDB
server](https://docs.indiscale.com/caosdb-server/README_SETUP.html)
and the [CaosDB
Webui](https://docs.indiscale.com/caosdb-webui/getting_started.html)
and start the server. If you want to use test data, you can use the
sql dump from the integration-test repository and restore it as
explained
[here](https://docs.indiscale.com/caosdb-server/administration/maintenance.html).

### Setup with LinkAhead ###

If you have access to an installation of caosdb-deploy/LinkAhead,
follow the instructions in `caosdb-deploy/README_SETUP.md` or install
the linkahead debian package. Choose the config file
`profile/profile.yml` as your profile, and start linkahead with that
selcetion..

**Note on local execution**: The `main` branch of this project is
configured to be run on the server that hosts the knowledge base. In
order to execute this locally on your own machine for testing,
debugging, or development, please check out the `local-testing-main`
branch instead. Afterwards you have to re-initialize the git
submodules in order to have the integration-tests submodule with
integration tests and test data. The same holds true for feature
branches which follow the naming convention of `f-<feature>` for the
server branch and `local-testing-<feature>` for local execution.

## Structure of this repository ##

Customized configurations of server and web interface, server-side
scripts and everything related to the simplified Django frontend (at
some later time) is located within the `profile` directory. Most
importantly, profile settings are configured in
`profile/profile.yml`. Here, you can also specify the branches or
specific commits of the CaosDB-components that will be used for
building the linkahead application when executing this using
caosdb-deploy. Customizations and extensions to the WebUI are stored
in `profile/custom/caosdb-server/caosdb-webui` and server-side scripts
are contained in `profile/custom/caosdb-server/scripting/bin`. Note
that scripts have to be made executable in order to be run as
server-side scripts.

Other scripts for the manual import of data, synchronization with
CrossRef etc. are located within the `scripts` directory. Filled in
tables with best practice rules can be saved in `rule_files`. The
filled in templates can be inserted using
`scripts/insert_data_from_template.py`. Adaptions to the data model,
apart from using the webinterface or the Python client, can be
configured in `scripts/datamodel.yml` and inserted using
`scripts/insert_datamodel.py`. See
[here](https://docs.indiscale.com/caosdb-advanced-user-tools/) for
more information on the syntax of the datamodel configuration file.

