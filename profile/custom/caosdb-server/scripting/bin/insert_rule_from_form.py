#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# ** header v3.0
#
# Copyright (C) 2021 University Medical Center Göttingen, Department
# of Medical Informatics
# Copyright (C) 2021 Florian Spreckelsen
# <florian.spreckelsen@med.uni-goettingen.de>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with this program. If not, see
# <https://www.gnu.org/licenses/>.
#
# ** end header
#
import json
import logging
import sys
from datetime import date

import caosdb as db
from caosadvancedtools.cfood import assure_object_is_in_list
from caosadvancedtools.guard import global_guard, UPDATE
from caosadvancedtools.serverside import helper
from caosadvancedtools.serverside.logging import configure_server_side_logging


def main():

    parser = helper.get_argument_parser()
    args = parser.parse_args()

    # check whether this is executed as server-side script or whether
    # it is executed locally.
    if hasattr(args, "auth_token") and args.auth_token:
        # has a non-empty auth token in case of server-side execution
        db.configure_connection(auth_token=args.auth_token)
        debug_file = configure_server_side_logging()
        logger = logging.getLogger("caosadvancedtools")

    else:
        # executed locally
        logger = logging.getLogger("caosadvancedtools")
        # print logging information in case of local execution
        logger.addHandler(logging.StreamHandler(sys.stdout))
        logger.setLevel(level=logging.DEBUG)
        debug_file = None

    with open(args.filename, "r") as fi:

        data = json.load(fi)

    rule_text = data["rule_text"]

    # check whether rule already exists
    try:
        candidates = db.execute_query(
            "FIND Rule WITH RuleText='{}'".format(rule_text))
        if candidates:
            logger.warning("There is already a rule with the exact same text. "
                           "Please use the existing one and change it using "
                           "the edit mode if necessary. Visit the existing rule "
                           "<a href=/Entity/{}>here</a>.".format(
                               candidates[0].id))
            return
    except db.CaosDBException as e:
        # continue if there were errors, e.g. due to too long rule
        # texts.
        logger.debug(e)

    # Get parents
    parent_query = "FIND RECORDTYPE Rule WITH " + " OR ".join(
        ["ID={}".format(parent_id) for parent_id in data["categories"]])
    parents = db.execute_query(parent_query)

    # allow updates
    global_guard.level = UPDATE

    rfps = {}
    rfp_rt = db.RecordType(name="RulesFromPublication").retrieve()
    for pub_id in data["publication"]:
        # look for analysis nodes and create one if it is missing
        rfp = db.execute_query(
            "FIND {} WHICH REFERENCES {}".format(rfp_rt.name, pub_id))
        if not rfp:
            rfp = db.Record().add_parent(rfp_rt)
            rfp.add_property(name="Publication", value=int(pub_id))
            rfp.insert()
        else:
            rfp = rfp[0]
        rfps[pub_id] = rfp

    for int_id in data["interpreter"]:
        # add interpreters to analysis nodes
        for rfp in rfps.values():
            assure_object_is_in_list(int(int_id), rfp, "Interpreter")

    rule = db.Record()
    for parent in parents:
        rule.add_parent(parent)
    rule.add_property(name="RuleText", value=rule_text)
    rule.add_property(name="Active", value=True)
    today = date.today()
    rule.add_property(name="CreatedOn", value=today)
    rule.add_property(name="EditedOn", value=today)
    if data["project"]:
        rule.add_property(name="Project",
                          value=[
                              int(proj_id) for proj_id in data["project"]],
                          datatype=db.LIST("Project"))
    rule.insert()

    for rfp in rfps.values():
        assure_object_is_in_list(rule, rfp, "Rule")

    logger.info("Your rule has been inserted successfully. "
                "Click <a href=/Entity/{}>here</a> to view it.".format(rule.id))


if __name__ == "__main__":

    main()
