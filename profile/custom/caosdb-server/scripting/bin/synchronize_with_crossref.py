#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# ** header v3.0
#
# Copyright (C) 2021 University Medical Center Göttingen, Department
# of Medical Informatics
# Copyright (C) 2021 Florian Spreckelsen
# <florian.spreckelsen@med.uni-goettingen.de>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with this program. If not, see
# <https://www.gnu.org/licenses/>.
#
# ** end header
#
import logging
import sys

import caosdb as db
from caosadvancedtools.cfood import (assure_object_is_in_list,
                                     assure_property_is)
from caosadvancedtools.guard import global_guard, UPDATE
from caosadvancedtools.serverside import helper
from caosadvancedtools.serverside.logging import configure_server_side_logging
from habanero import Crossref
from requests import HTTPError

# Giving a mail address is considered nicer by the Crossref people so
# that they can contact us if anything goes wrong (or ban a certain
# mail in case of creating an absurde load on their free, public
# servers). See https://github.com/CrossRef/rest-api-doc#etiquette.
CROSSREF_POLITE_MAIL = "medinf.num-wissensbasis@med.uni-goettingen.de"


def _datestring_from_dateparts(date_parts):
    """Converts a dateparts object from the Crossref response to a date
    string in the format `yyyy-mm-dd`. Month and day may be omitted.

    """
    # date_parts is a list of lists (with only one element)
    date_parts = date_parts[0]
    datestr = str(date_parts[0])
    if len(date_parts) > 1:
        month = str(date_parts[1])
        if len(month) == 1:
            month = "0" + month
        datestr += "-" + month
    if len(date_parts) == 3:
        day = str(date_parts[2])
        if len(day) == 1:
            day = "0" + day
        datestr += "-" + day

    return datestr


def _find_or_insert_affiliation(affiliation, affiliation_rt):

    candidates = db.execute_query("FIND {} WITH name=\"{}\"".format(
        affiliation_rt.name, affiliation["name"]))

    if not candidates:
        # Affiliation doesn't exist in db
        aff = db.Record(name=affiliation["name"])
        aff.add_parent(affiliation_rt).insert()

        return aff

    elif len(candidates) == 1:
        # Affiliation exists and is unique
        return candidates[0]

    # Improbable case of (at least) two affiliation records with the
    # exact same name: Skip this affiliation altogether.
    return None


def _update_or_create_author(author, affiliation_rt, author_rt,
                             lastname_prop, firstname_prop,
                             to_be_updated=None):

    if not "family" in author:
        # Skip authors without names
        return None

    query = "FIND {} WITH {}=\"{}\"".format(
        author_rt.name, lastname_prop.name, author["family"])
    has_given = "given" in author and author["given"]
    if has_given:
        query += " AND {}=\"{}\"".format(firstname_prop.name, author["given"])

    candidates = db.execute_query(query)

    if not candidates:
        # Author doesn't exist in db
        recname = author["family"]
        if has_given:
            recname += ", " + author["given"]
        auth = db.Record(name=recname).add_parent(author_rt)
        auth.add_property(name=lastname_prop.name, value=author["family"])
        if has_given:
            auth.add_property(name=firstname_prop.name, value=author["given"])
        auth.insert()

    elif len(candidates) == 1:
        # author exists and is unique
        auth = candidates[0]

    else:
        # author isn't unique
        warn("Query '{}' did return more than one author.")
        return None

    if "affiliation" in author:

        affiliations = [_find_or_insert_affiliation(aff, affiliation_rt)
                        for aff in author["affiliation"]]

        assure_object_is_in_list(
            [aff for aff in affiliations if aff], auth, affiliation_rt.name, to_be_updated=to_be_updated)

    return auth


def _update_publication_authors_and_affiliations(pub_rec,
                                                 crossref_entry,
                                                 affiliation_rt,
                                                 author_rt,
                                                 lastname_prop,
                                                 firstname_prop,
                                                 to_be_updated=None):
    """Update a publication record with information obtained from
    crossref.

    """

    # Title is a list in crossref's API
    title = crossref_entry["title"][0]
    assure_property_is(pub_rec, "Title", title, to_be_updated=to_be_updated)
    if "author" in crossref_entry:

        authors = [_update_or_create_author(
            auth, to_be_updated=to_be_updated, author_rt=author_rt,
            lastname_prop=lastname_prop, firstname_prop=firstname_prop,
            affiliation_rt=affiliation_rt)
            for auth in crossref_entry["author"]]
        assure_property_is(pub_rec, author_rt.name, [
                           author.id for author in authors if author],
                           datatype=db.LIST(author_rt.name),
                           to_be_updated=to_be_updated)

    # This may or may not include month and day, but will always have
    # at least year.
    date = crossref_entry["issued"]["date-parts"]
    assure_property_is(pub_rec, "Date", _datestring_from_dateparts(date),
                       to_be_updated=to_be_updated)

    assure_property_is(pub_rec, "NumberOfCitations",
                       crossref_entry["is-referenced-by-count"], to_be_updated=to_be_updated)


def main():

    # configure connection logging
    conlogger = logging.getLogger("connection")
    conlogger.setLevel(level=logging.ERROR)

    parser = helper.get_argument_parser()
    args = parser.parse_args()
    # check whether this is executed as server-side script or whether
    # it is executed locally.
    if hasattr(args, "auth_token") and args.auth_token:
        # has a non-empty auth token in case of server-side execution
        db.configure_connection(auth_token=args.auth_token)
        debug_file = configure_server_side_logging()
        logger = logging.getLogger("caosadvancedtools")

    else:
        # executed locally
        logger = logging.getLogger("caosadvancedtools")
        # print logging information in case of local execution
        logger.addHandler(logging.StreamHandler(sys.stdout))
        logger.setLevel(level=logging.DEBUG)
        debug_file = None

    # initialize crossref with the mailto parameter set above
    cr = Crossref(mailto=CROSSREF_POLITE_MAIL)

    # get all publications with DOIS
    pubs = db.execute_query("FIND RECORD Publication WHICH HAS A DOI")
    num_updated = len(pubs)

    # collect necessary recordtypes and properties globally for
    # performance reasons.
    affiliation_rt = db.RecordType(name="Affiliation").retrieve()
    author_rt = db.RecordType(name="Author").retrieve()
    lastname_prop = db.Property(name="Lastname").retrieve()
    firstname_prop = db.Property(name="Firstname").retrieve()

    for pub in pubs:

        # better safe than sorry, check whether publication actually
        # has a DOI
        doi_prop = pub.get_property("DOI")
        if doi_prop is not None and doi_prop.value:

            doi = doi_prop.value
            # check if someone entered more than one DOI (should never
            # be the case, so warn about this).
            if isinstance(doi, list):
                if len(doi) > 1:
                    logger.warning(
                        "The publication record with id={} seems to "
                        "have more than one DOI. Please check whether "
                        "this is actually desired. Only the first entry "
                        "in the list of DOIs is used for CrossRef "
                        "synchronization.".format(pub.id)
                    )
                doi = doi[0]
            try:
                crossref_entry = cr.works(ids=[doi])
                _update_publication_authors_and_affiliations(
                    pub, crossref_entry["message"],
                    affiliation_rt=affiliation_rt,
                    author_rt=author_rt,
                    lastname_prop=lastname_prop,
                    firstname_prop=firstname_prop
                )
            except HTTPError:
                # Crossref couldn't find the DOI
                logging.debug("Couldn't find CrossRef entry for "
                              "publication {}".format(pub.id))
                # didn't update this one
                num_upadted -= 1
        else:
            # didn't have a non-empty DOI
            logging.debug(
                "Publication {} has an empty DOI value.".format(pub.id))
            # didn't update this one
            num_update -= 1

    logger.info(
        "Successfully synchronized {} publications with "
        "valid DOIs.".format(num_updated)
    )


if __name__ == "__main__":
    global_guard.level = UPDATE
    main()
