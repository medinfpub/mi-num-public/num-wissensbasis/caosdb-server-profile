#!/usr/bin/env python3
# encoding: utf-8
#
# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2020 University Medical Center Göttingen, Department
# of Medical Informatics
# Copyright (C) 2020 Florian Spreckelsen
# <florian.spreckelsen@med.uni-goettingen.de>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with this program. If not, see
# <https://www.gnu.org/licenses/>.
#
# ** end header #
import logging
import os
import sys

import caosdb as db
from caosadvancedtools.serverside import helper
from caosadvancedtools.serverside.logging import configure_server_side_logging
from caosadvancedtools.webui_formatter import WebUI_Formatter


def get_parser():
    """Use standard SSS parser and add an argument for the record id of
    the record to be evaluated.

    """
    par = helper.get_argument_parser()
    par.add_argument("record_id",
                     help="The id of the record to be evaluated.")

    return par


def main():
    """Doesn't really do anything atm."""
    conlogger = logging.getLogger("connection")
    conlogger.setLevel(level=logging.ERROR)

    parser = get_parser()
    args = parser.parse_args()

    # when executed as SSS, there is an authentication token in the
    # arguments:
    if hasattr(args, "auth_token") and args.auth_token:
        # configure connection and logging for server-side execution
        db.configure_connection(auth_token=args.auth_token)
        debug_file = configure_server_side_logging()
        logger = logging.get_Logger("caosadvancedtools")
    else:
        # logging config for local execution; no changes necessary in
        # connection config.
        logger = logging.getLogger("caosadvancedtools")
        logger.addHandler(logging.StreamHandler(sys.stdout))
        # local execution is probably for development/debugging
        # anyway:
        logger.setLevel(logging.DEBUG)
        debug_file = None

    record_id = args.record_id
    logger.info("Evaluation for record {}".format(record_id))


if __name__ == "__main__":
    main()
