#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# ** header v3.0
#
# Copyright (C) 2021 University Medical Center Göttingen, Department
# of Medical Informatics
# Copyright (C) 2021 Florian Spreckelsen
# <florian.spreckelsen@med.uni-goettingen.de>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with this program. If not, see
# <https://www.gnu.org/licenses/>.
#
# ** end header
#
import json
import logging
import sys

import caosdb as db
from caosadvancedtools.serverside import helper
from caosadvancedtools.serverside.logging import configure_server_side_logging


def main():

    parser = helper.get_argument_parser()
    args = parser.parse_args()

    # check whether this is executed as server-side script or whether
    # it is executed locally.
    if hasattr(args, "auth_token") and args.auth_token:
        # has a non-empty auth token in case of server-side execution
        db.configure_connection(auth_token=args.auth_token)
        debug_file = configure_server_side_logging()
        logger = logging.getLogger("caosadvancedtools")

    else:
        # executed locally
        logger = logging.getLogger("caosadvancedtools")
        # print logging information in case of local execution
        logger.addHandler(logging.StreamHandler(sys.stdout))
        logger.setLevel(level=logging.DEBUG)
        debug_file = None

    with open(args.filename, "r") as fi:

        data = json.load(fi)

    if "selected_cats" in data and data["selected_cats"]:
        query_base = "FIND RECORDTYPE Rule WITH "
        selected_names = [
            rt.name for rt in db.execute_query(
                query_base + " OR ".join(
                    ["ID={}".format(parent_id)
                     for parent_id in data["selected_cats"]])
            )
        ]
    else:
        selected_names = ["Rule"]

    rule_ids = set()
    for name in selected_names:

        tmp_set = {rule.id for rule in db.execute_query(
            "FIND RECORD '{}'".format(name))}
        rule_ids = rule_ids.union(tmp_set)

    if "excluded_cats" in data and data["excluded_cats"]:

        excluded_names = [
            rt.name for rt in db.execute_query(
                query_base + " OR ".join(
                    ["ID={}".format(parent_id)
                     for parent_id in data["excluded_cats"]])
            )
        ]
        for name in excluded_names:

            tmp_set = {rule.id for rule in db.execute_query(
                "FIND RECORD '{}'".format(name))}
            rule_ids = rule_ids.difference(tmp_set)

    if "projects" in data and data["projects"]:

        project_query = "FIND Rule WHICH REFERENCES " + \
            "{} ".join(data["projects"])
        tmp_set = {rule.id for rule in db.execute_query(project_query)}
        rule_ids = rule_ids.intersection(tmp_set)

    link_text = "/Entity/" + '&'.join(str(rid) for rid in rule_ids)
    if rule_ids:
        logger.info(
            """Found {} best-practice rules.
<p><a href={} class="btn btn-1 btn-primary">Show rules</a>
</p>""".format(len(rule_ids), link_text)
        )
    else:
        logger.info("Found no rules matching the above criteria.")


if __name__ == "__main__":

    main()
