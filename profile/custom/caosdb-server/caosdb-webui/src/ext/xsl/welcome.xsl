<!--
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2019 IndiScale GmbH
 * Copyright (C) 2019 Daniel Hornung (d.hornung@indiscale.com)
 * Copyright (C) 2021 University Medical Center Göttingen, Department of Medical Informatics
 * Copyright (C) 2021 Florian Spreckelsen <florian.spreckelsen@med.uni-goettingen.de> 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html"/>
  <xsl:template name="welcome">
    <div class="caosdb-v-welcome-panel mt-auto">
      <div class="container pb-5 p-3 caosdb-f-welcome-panel" style="border-radius: 5px;">
      <h1> Herzlich Willkommen</h1>
      <p>Diese Wissensbasis bietet Ihnen Best Practices sowie Fakten und Maßnahmen zum Verständnis und der Bewältigung der COVID-19 Pandemie an, die im Rahmen des <a href="https://www.netzwerk-universitaetsmedizin.de/">Netzwerks Universitätsmedizin</a> (NUM) entwickelt wurden. </p>
      <p>Die Plattform wurde entwickelt, um Informationen aus verschiedenen Wissensdomains zu verwalten. Aktuell finden Sie hier die Ergebnisse der folgenden NUM Projekte:  </p>

     <br/>

   <p align="center"> 
    <a href="https://num-compass.science"> <img src="webinterface/pics/NUM-Compass-Logo.png" 
	  alt="Compass" class="welcome-logo"/> </a> <xsl:text>&#x3000;</xsl:text> <xsl:text>&#x3000;</xsl:text> <xsl:text>&#x3000;</xsl:text> <xsl:text>&#x3000;</xsl:text>
     <a href="https://www.umg.eu/forschung/corona-forschung/num/b-fast/projekt-b-fast/"><img  src="webinterface/pics/NUM-B-FAST-Logo.png"	  alt="B-FAST" class="welcome-logo"/> </a> <xsl:text>&#x3000;</xsl:text> <xsl:text>&#x3000;</xsl:text> <xsl:text>&#x3000;</xsl:text>
    <a href="http://egepan.de/"><img src="webinterface/pics/egepan-logo.png" 
	  alt="egePan Unimed" class="welcome-logo" style="height: 90px;" /> </a>
  </p>
       <p></p>

   
      <h2>COMPASS</h2>
    <p> <b>Coordination On Mobile Pandemic Apps best practice and Solution Sharing</b> </p>
       
  <p> Das Projekt <a href="https://num-compass.science/de/"> COMPASS </a> unterstützt Wissenschaftler:innen
  bei der forschungskompatiblen Entwicklung von Pandemieapps und appbasieren Pandemie-Studien. 
  Sie finden vielfältige Best Practices zu verschiedenen Aspekten, wie z.B.    
   <a href="https://num.umg.eu/Entity/?P=0L10&amp;query=find%20record%20ethik">Ethik</a>, 
   <a href="https://num.umg.eu/Entity/?P=0L10&amp;query=FIND%20RECORD%20%22Vertrauen%20und%20Akzeptanz%22"> Vertrauen und Akzeptanz</a>,
    <a href="https://num.umg.eu/Entity/?P=0L10&amp;query=find%20record%20nachhaltigkeit"> Nachhaltigkeit </a>und
     <a href="https://num.umg.eu/Entity/?P=0L10&amp;query=find%20record%20Interoperabilit%C3%A4t"> Interoperabilität</a>.
      Hier finden Sie alle <a href="/Entity/?query=FIND Rule WITH Project='Compass'">Best Practices aus dem COMPASS Projekt</a>. 
      </p>
      

     <h2>B-FAST</h2>
     <p> <b>Bundesweites Forschungsnetz Angewandte Surveillance und Testung</b> </p>  

<p> Das Projekt 	  <a href="https://www.umg.eu/forschung/corona-forschung/num/b-fast/projekt-b-fast/"> B-FAST </a> entwickelt Test- und Surveillancestrategien für unterschiedliche Settings, beispielsweise Gesamtbevölkerung, Schulen und Kitas, Risikobereiche und Kliniken. Sie finden Fakten aus einer Befragung von universitären und nichtuniversitären Kliniken zu verschiedenen Aspekten der Surveillance, wie z.B.
  
   <a href="https://num.umg.eu/Entity/?P=0L10&amp;query=FIND%20RECORD%20Patienten">Patient:innen</a>, 
<a href="https://num.umg.eu/Entity/?P=0L10&amp;query=FIND%20RECORD%20%22Test-%20und%20Surveillancestrategien%22"> Test- und Surveillancestrategien</a>,  
<a href="https://num.umg.eu/Entity/?P=0L10&amp;query=FIND%20RECORD%20Infektioskontrolle"> Infektionskontrolle </a>und 
<a href="https://num.umg.eu/Entity/?P=0L10&amp;query=find%20record%20%22Alle%20Mitarbeitende%22"> Mitarbeitende</a>. 
Hier finden Sie eine Übersicht der <a href="/Entity/?query=FIND Rule WITH Project='B-FAST'">Fakten aus dem B-FAST Projekt</a>. 

</p>

<h2> egePan Unimed</h2>
 <p> <b>Entwicklung, Testung und Implementierung von regional adaptiven Versorgungsstrukturen und Prozessen für ein evidenzgeleitetes Pandemiemanagement koordiniert durch die Universitätsmedizin</b> </p>
  <p> Das Projekt <a href="http://egepan.de/"> egePan Unimed </a> hat sich den Aufbau eines Pandemiemanagements 
      und einer Pandemic Preparedness mit den jeweiligen Universitätskliniken als Supra-Maximalversorger in Kooperation und in Abstimmung mit den für das Pandemiemanagement 
	  verantwortlichen Einrichtungen zum Ziel gesetzt. Weiteres Ziel ist ein optimiertes regionales und föderales Pandemiemanagement unter 
	  Berücksichtigung des teilweise landesindividuellen komplexen Geflechts des Gesundheitssystems. 
    Sie finden Maßnahmen aus vielfältigen Bereichen, wie z.B.
     <a href="https://num.umg.eu/Entity/?P=0L10&amp;query=FIND%20RECORD%20Unternehmenskultur"> Unternehmenskultur</a>,
     <a href="https://num.umg.eu/Entity/?P=0L10&amp;query=FIND%20RECORD%20Rahmenbedingungen"> organisationale und soziale Rahmenbedingungen</a>, 
<a href="https://num.umg.eu/Entity/?P=0L10&amp;query=FIND%20RECORD%20Pr%C3%A4ventionsma%C3%9Fnahmen"> Präventionsmaßnahmen</a> und
<a href="https://num.umg.eu/Entity/?P=0L10&amp;query=FIND%20RECORD%20%27Hilfs-%20und%20Behandlungsangebot%27"> Hilfs- und Behandlungsangebote</a>.

    Hier finden Sie alle <a href="https://num.umg.eu/Entity/?P=0L10&amp;query=FIND%20record%20which%20references%201323">Maßnahmen aus egePan Unimed</a>. </p>

 <p></p>
 
<br/><br/>
 
      <h1>Kurzanleitung</h1>
<p>    Für den schnellen Einstieg nutzen Sie am besten die Funktion „Filter best practices“, welche Sie oben in der Menüleiste auswählen können.
    Um die Einträge zu durchsuchen wählen sie zunächst eins der Projekte aus und danach beliebig viele Keywords.  </p>
  
  <p> Über das Feld ‚Query‘ können Sie eigene Suchanfragen eingeben, Sie finden aber unterhalb des Suchfelds
  auch einige vorgefertigte Suchanfragen, sowie auch das Glossar aller verfügbaren Best-Practice-Kategorien und Schlagwörter.  </p>
  
   <p> Die Wissensbasis ist implementiert in dem Open Source Datenmanagementsystem <a href="https://caosdb.org">CaosDB</a>,
  das eine umfangreiche Abfragesprache CQL bietet. Mehr Informationen zur Suchsprache CQL finden Sie in der <a href="https://docs.indiscale.com/caosdb-webui/tutorials/query.html">Dokumentation</a>.
  </p>

      <h1>Kontakt und Support</h1>
      <p>Falls Sie Fragen, Anmerkungen oder sonstiges Feedback haben,
       senden Sie sie bitte an <a
      href="mailto:medinf.num-wissensbasis@med.uni-goettingen.de">medinf.num-wissensbasis@med.uni-goettingen.de</a>.</p>
      
       <p></p>

      <p align="right"> 
	<a href="https://num-compass.science">
	  <img src="webinterface/pics/NUM-Compass-Logo.png" 
	       alt="Compass" class="welcome-logo"/>
	</a>
  <a href="https://www.umg.eu/forschung/corona-forschung/num/b-fast/projekt-b-fast/">
	  <img  src="webinterface/pics/NUM-B-FAST-Logo.png" 
		alt="B-FAST" class="welcome-logo"/>
	</a>
  	<a href="http://egepan.de/">
	  <img src="webinterface/pics/egepan-logo.png"
	       alt="egePan Unimed" class="welcome-logo" style="height: 90px;"/>
	</a>
	<a href="https://netzwerk-universitaetsmedizin.de">
	  <img src="webinterface/pics/NUM-Logo.png" alt="Netzwerk Universitätsmedizin"
	       class="welcome-logo" style="height: 80px;"/>
	</a>
  <a href="https://www.umg.eu/">
	  <img src="webinterface/pics/Universitaetsmedizin-Logo.png" alt="Universitätsmedizin Göttingen"
	     class="welcome-logo"/>
	</a>

      </p>
    </div>
    </div>
  <script>
    if(isAuthenticated()){
      $('.caosdb-f-welcome-panel h1').text('Hallo, ' + getUserName() + "!");
    }
  </script>
  </xsl:template>
</xsl:stylesheet>
