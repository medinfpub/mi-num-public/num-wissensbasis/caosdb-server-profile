/*  
 * ** header v3.0  
 * This file is a part of the CaosDB Project.
 * 
 * Copyright (C) 2021 University Medical Center Göttingen, Department
 * of Medical Informatics
 * Copyright (C) 2021 Florian Spreckelsen
 * <florian.spreckelsen@med.uni-goettingen.de>  
 *  
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 *  
 * You should have received a copy of the GNU Affero General Public
 * License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 *  
 * ** end header  
 */

"use strict";


// Adapt the navbar and add curator tools for legged-in users. Also
// hide the file system since it's not used anyway.    
var compass_interface = new function() {

    this.init = function() {

	compass_interface.simplify();
	compass_interface.filter_rules();

	if (isAuthenticated()) {

	    /*
	     * If we ever need the file system for curators, uncomment
	     * the following.
	     */
	    // $("#caosdb-navbar-filesystem").show();

	    // add new rule button
	    compass_interface.insert_new_rule();
	    
	    // add crossref sync button
	    compass_interface.sync_with_crossref();
	}

    }

    this.simplify = function() {

	// hide file system button
	$("#caosdb-navbar-filesystem").hide();

    }


    // Create the button for crossref sync and add it to the "curator
    // tools"
    this.sync_with_crossref = function() {

	var init = function () {

	    const script = "synchronize_with_crossref.py";
	    const button_name = "Synchronize publications";
	    const title = "Update all publication with valid DOIs with information recieved from CrossRef.";

	    // first create form that will be shown in the modal ...
	    const crossref_form = make_scripting_caller_form(script, button_name);
	    // ... then the actual modal around this
	    const modal = make_form_modal(crossref_form);

	    // add the corresponding button to the curator tools
	    navbar.add_tool(
		button_name, "Curator tools",
		{
		    title: title,
		    callback: () => {
			$(modal).modal("toggle");
		    }
		}
	    );
	    
	}
	
	var make_form_modal = function (form) {

	    const title = "Synchronize publications with CrossRef";
	    const modal = $(`<div class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">${title}</h4> 
                <button type="button"
                  class="btn-close"
                  data-bs-dismiss="modal"
                  aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <p>Please note that depending on the load on the CrossRef server, this may take a while.</p>
            </div>
        </div>
    </div>
</div>`
	    );

	    // append the actual form with the call button
	    modal.find(".modal-body").append(form);

	    return modal[0];
	}
	
	var make_scripting_caller_form = function (script, button_name) {

	    const scripting_caller = $(`<form method=POST action="/scripting">
    <input type="hidden" name="call" value="${script}"/>
    <input type="hidden" name="-p0" value=""/>
    <div class="form-group">
        <input type="submit" class="form-control btn btn-primary" value="${button_name}"/>
    </div>
</form>`
				      );

	    return scripting_caller[0];

	}

	init();

    }


    // Create input form for creation of new rules
    this.insert_new_rule = function() {

	var init = function () {

	    const script = "insert_rule_from_form.py";
	    const button_name = "Insert new rule";
	    const title = "Enter a new best practice rule and choose from existing categories, projects, and sources that will be assigned to it.";
	    
	    const submission_form = make_submission_form(script);

	    var modal = make_form_modal(submission_form);

	    navbar.add_tool(
		button_name, "Curator tools",
		{
		    title: title,
		    callback: () => {
			$(modal).modal("toggle");
		    }
		}
	    );
	}

	var make_form_modal = function (form) {
	    const title = "Insert a new best-practice rule.";
	    const modal = $(`<div class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">${title}</h4> 
                <button type="button"
                  class="btn-close"
                  data-bs-dismiss="modal"
                  aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <p>Please enter the recommendation in the text field below and choose at least one category. You may also chose the project(s), source(s), and interpreter(s) of this rule. Please note that you can only chose from existing categories, projects, sources and interpreters. If you want to select new entries you ave to create them using the edit mode first.</p>
            </div>
        </div>
    </div>
</div>`
	    );

	    // append the actual form with the call button
	    modal.find(".modal-body").append(form);

	    // don't need the cancel button
	    modal.find(".caosdb-f-form-elements-cancel-button").hide();

	    return modal[0];
	}

	var make_submission_form = function (script) {

	    const form_config = {
		script: script,
		fields: [
		    {
			name: "rule_text",
			label: "Recommendation",
			type: "text",
			required: true
		    },
		    {
			name: "categories",
			label: "Categories",
			type: "reference_drop_down",
			multiple: true,
			required: true,
			query: "FIND RECORDTYPE Rule",
			make_desc: getEntityName
		    },
		    {
			name: "project",
			label: "Project(s)",
			type: "reference_drop_down",
			multiple: true,
			required: false,
			query: "FIND RECORD Project",
			make_desc: getEntityName
		    },
		    {
			name: "publication",
			label: "Source(s)",
			type: "reference_drop_down",
			multiple: true,
			required: false,
			query: "FIND RECORD Publication",
			make_desc: (e) => {
			    return getProperty(e, "Title");
			}
		    },
		    {
			name: "interpreter",
			label: "Interpreter(s)",
			type: "reference_drop_down",
			multiple: true,
			required: false,
			query: "FIND RECORD Interpreter",
			make_desc: getEntityName
		    }
		]
	    };

	    const form = form_elements.make_form(form_config);

	    return form;
	}

	init();
    }

    // Create filter form next to curator tools
    this.filter_rules = function() {

	var init = function() {

	    const script = "filter_rules.py";
	    const button_name = "Filter best practices";
      var button = $(`<a class="nav-link" role="button">${button_name}</a>`)[0];
	    const title = "Select the categories the best practices of which you want to show.";

	    const filter_form = make_filter_form(script);
	    const modal = make_filter_modal(filter_form);

	    // append button to navbar
	    navbar.add_button(
        button,
		{
		    title: title,
		    callback: () => {
                $(modal).modal("toggle");
		    }
		}
	    );
	}

	var make_filter_form = function (script) {

	    const form_config = {
		script: script,
		fields: [
		    {
			name: "selected_cats",
			label: "Keywords",
			type: "reference_drop_down",
			required: false,
			multiple: true,
			query: "FIND RECORDTYPE Rule",
			make_desc: getEntityName
		    },
		    {
			name: "excluded_cats",
			label: "Except",
			type: "reference_drop_down",
			required: false,
			multiple: true,
			query: "FIND RECORDTYPE Rule",
			make_desc: getEntityName
		    },
		    {
			name: "projects",
			label: "Project(s)",
			type: "reference_drop_down",
			required: false,
			multiple: true,
			query: "FIND RECORD Project",
			make_desc: getEntityName
		    }
		],
	    };

	    const form = form_elements.make_form(form_config);

	    return form;
	}

	var make_filter_modal = function (form) {

	    const title = "Filter best-practice rules by category";
	    const modal = $(`<div class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">${title}</h4> 
                <button type="button"
                  class="btn-close"
                  data-bs-dismiss="modal"
                  aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <p>Select the categories the rules of which will be shown. If none is selected, all will be shown. Optionally, you can exclude categories from the results. You may also select the project(s) to which the rules should belong.  If no project is selected, the recommendations of all projects will be shown.</p>
            </div>
        </div>
    </div>
</div>`
			   );
	    
	    // append the actual form
	    modal.find(".modal-body").append(form);
	    
	    // don't need the cancel button
	    modal.find(".caosdb-f-form-elements-cancel-button").hide();

	    // change text of submit button
	    modal.find(".caosdb-f-form-elements-submit-button").text("Apply filters");

	    return modal[0];
	}

	init();
    }
}

/**  
 * Add the extensions to the webui.  
 */
$(document).ready(function() {
    compass_interface.init();
});
